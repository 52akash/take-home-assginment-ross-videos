package test;

import main.Frame;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FrameTest {
    Frame frame;

    @org.junit.jupiter.api.BeforeEach
    protected void setUp() {
        frame = new Frame();
    }

    @Test
    void throwOneBallInAFrame(){
        // GIVEN no balls have been bowled in a frame
        // WHEN one ball is thrown BUT not all pins are knocked down
        frame.roll(2);
        // THEN my frame score is the number of pins I have knocked down.
        assertEquals(2, frame.score());
    }

    @Test
    void throwSecondBallInAFrame() {
        // GIVEN one ball has been bowled in a frame
        frame.roll(4);
        // WHEN a second ball is thrown BUT not all pins are knocked down
        frame.roll(3);
        // THEN my frame score is the number of pins I have knocked down with both balls.
        assertEquals(7, frame.score());
    }

    @Test
    void throwASpareInAFrame(){
        // GIVEN one ball has been bowled in a frame BUT not all pins have been knocked down
        frame.roll(5);
        // WHEN the second ball is thrown AND all remaining pins are knocked down
        frame.roll(5);
        // THEN the frame is marked as a spare
        assertTrue(frame.isSpare());
        // AND the frame score is the number of pins knocked down.
        assertEquals(10, frame.score());
    }

    @Test
    void throwStrikeInAFrame(){
        // GIVEN no balls have been bowled in a frame
        // WHEN one ball is thrown AND all pins are knocked down
        frame.roll(10);
        // THEN my frame is marked as a strike
        assertTrue(frame.isStrike());
        // AND the frame score is the number of pins knocked down.
        assertEquals(10, frame.score());
    }

}
