package test;

import main.BowlingGame;
import main.Frame;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BowlingGameTest {
    BowlingGame game;

    @org.junit.jupiter.api.BeforeEach
    protected void setUp() {
        game = new BowlingGame();
    }
    private void rollMany(int n, int pins) {
        for (int i = 0; i < n; i++) {
            game.roll(pins);
        }
    }
    private void rollSpare() {
        game.roll(5);
        game.roll(5);
    }

    private void rollStrike() {
        game.roll(10);
    }

    @Test
    void scoreASpare() {
        // GIVEN a frame is marked as a spare
        rollSpare();
        // WHEN the next ball is thrown
        game.roll(3);
        // THEN the score of that ball is added to the spare score.
        assertEquals(16, game.score());
    }


    @Test
    void scoreAStrike(){
        // GIVEN a frame is marked as a strike
        rollStrike();
        // WHEN the next two balls are thrown
        game.roll(3);
        game.roll(4);
        // THEN the score of those two balls is added to the strike score.
        assertEquals(24, game.score());
    }

    @Test
    void commitAFoul(){
        //  WHEN a ball is thrown
        game.roll(0);
        //  AND the foul line is crossed
	    // THEN the score of that ball is zero.
        assertEquals(0, game.score());
    }


    @Test
    void gameScore(){
        //  GIVEN a game is in progress
        rollStrike();
        rollMany(10, 3);
        rollStrike();
        rollSpare();
        //  WHEN the score is queried
        int score = game.score();
        //  THEN the game score is the sum of all scores for completed frames in the string.
        assertEquals(76, score);
    }


    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }
}
