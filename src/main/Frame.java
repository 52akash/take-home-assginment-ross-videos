package main;

    public class Frame {
        private static final int MAX_NUM_OF_PINS = 10;
        private static final int MAX_NUM_OF_ROLLS = 2;
        private final int[] rolls = new int[MAX_NUM_OF_ROLLS];
        private int rollCounter = 0;

        private int noOfPinsLeft = MAX_NUM_OF_PINS;

        public void roll(int noOfPins) throws RuntimeException{
            if(isAValidRoll(noOfPins) || isDone()){
                throw new FrameException("Invalid roll");
            }
            rolls[rollCounter] = noOfPins;
            noOfPinsLeft -= noOfPins;

            if (rollCounter == 0 && isStrike()){
                rollCounter = MAX_NUM_OF_ROLLS-1;
            } else {
                rollCounter++;
            }
        }

        private boolean isAValidRoll(int noOfPins) {
            return noOfPins < 0 || noOfPins > noOfPinsLeft;
        }

        public boolean isDone() {
            return isStrike() ||
                    rollCounter >= MAX_NUM_OF_ROLLS;
        }

        public boolean isStrike() {
            return rolls[0] == MAX_NUM_OF_PINS;
        }

        public int score() {
            if(isStrike() || isSpare()) return MAX_NUM_OF_PINS;

            int score = 0;
            for (int rollIndex = 0; rollIndex < rollCounter; rollIndex++) {
                score += rolls[rollIndex];
            }
            return score;
        }

        public boolean isSpare() {
            return rolls[0] + rolls[0] == MAX_NUM_OF_PINS;
        }

        public int getFirstRoll() {
            return rolls[0];
        }

        public class FrameException extends RuntimeException {
            public FrameException(String message){
                super(message);
            }
        }
    }
