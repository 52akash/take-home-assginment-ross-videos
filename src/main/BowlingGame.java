package main;

public class BowlingGame {
    private static final int MAX_NUMBER_OF_FRAMES = 10;
    private final Frame[] frames = new Frame[MAX_NUMBER_OF_FRAMES];
    private int frameCounter = 0;
    public BowlingGame(){
        for (int frameIndex = 0; frameIndex < MAX_NUMBER_OF_FRAMES; frameIndex++) {
            frames[frameIndex] = new Frame();
        }
    }
    public void roll(int noOfPins) {
        Frame frameToRoll = getFrame(frameCounter);
        if(frameToRoll.isDone()){
            frameToRoll = getFrame(++frameCounter);
        }
        frameToRoll.roll(noOfPins);
    }

    public int score() {
        int total = 0;

        for (int frameIndex = 0; frameIndex <= frameCounter; frameIndex++) {
            Frame frame = getFrame(frameIndex);
            total += frame.score();
            if(frame.isStrike()) {
                total += strikeBonus(frameIndex);
            } else if(frame.isSpare()) {
                total += spareBonus(frameIndex);
            }
        }
        return total;
    }

    private Frame getFrame(int frameIndex) {
        if(frameCounter >= MAX_NUMBER_OF_FRAMES){
            throw new RuntimeException("Out of index");
        }
        return frames[frameIndex];
    }

    private int spareBonus(int frameIndex) {
        try{
            return getFrame(frameIndex + 1).getFirstRoll();
        } catch (Exception ex){
            return 0;
        }
    }

    private int strikeBonus(int frameIndex) {
        try{
            return getFrame(frameIndex + 1).score();
        } catch (Exception ex){
            return 0;
        }
    }
}
